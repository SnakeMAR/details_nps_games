require 'open-uri'
require 'json'
require 'csv'
require './GameDetail.rb'
require 'active_record'

games = CSV.parse(open("http://nopaystation.com/tsv/PSV_GAMES.tsv"), { :col_sep => "\t", :headers => true })
dlcs = CSV.parse(open("http://nopaystation.com/tsv/PSV_DLCS.tsv"), { :col_sep => "\t", :headers => true, :quote_char => "|" })
themes = CSV.parse(open("http://nopaystation.com/tsv/PSV_THEMES.tsv"), { :col_sep => "\t", :headers => true, :quote_char => "|" })

#https://store.playstation.com/valkyrie-api/en/CA/19/resolve/UP1024-PCSE01279_00-EXPHORROR2017HRV?depth=2
regions = {
  "US" => ["en/CA", "en/US"],
  "ASIA" => ["en/HK", "en/SG", "en/TW", "zh/CN", "ko/KR"],
  "JP" => ["ja/JP"],
  "EU" => ["en/GB", "fr/FR"],
  "INT" => [],
}

games.each do |game|
  if GameDetail.where(:contentId => game["Content ID"]).exists?
    next
  end

  tries = 5
  games_info = ""
  regions[game["Region"]].each do |one_region|
    begin
      games_info = JSON.parse(open("https://store.playstation.com/valkyrie-api/#{one_region}/25/resolve/#{game["Content ID"]}?depth=2").read)
    rescue => e
      if e.message == "404 Not Found"
        next
      end
      tries -= 1
      sleep 5
      retry if tries != 0
      exit
    end
  end

  if games_info == ""
    `echo #{game["Title ID"]} >> Bad_games.txt`
    next
  end

  game_info = games_info["included"][0]
  psn_addOns = games_info["included"].select {|item| item["attributes"]["game-content-type"] == "Add-On" }.collect{|i| i["id"] }
  psn_themes = games_info["included"].select {|item| item["attributes"]["game-content-type"] == "Theme" }.collect{|i| i["id"] }

  addOns = ((dlcs.select {|i| i["Title ID"] == game["Title ID"] }.collect {|i| i["Content ID"]}) + psn_addOns).uniq
  themes = ((themes.select {|i| i["Title ID"] == game["Title ID"] }.collect {|i| i["Content ID"]}) + psn_themes).uniq

  game_result = GameDetail.new(
              description: game_info["attributes"]["long-description"],
              rating: game_info["attributes"]["content-rating"]["url"],
              addOns: addOns,
              themes: themes,
              contentId: game["Content ID"],
              region: game["Region"],
              contentType: game_info["attributes"]["game-content-type"],
              consoleType: "PSV",
              fileType: "GAMES",
              genres: game_info["attributes"]["genres"],
              languages: game_info["attributes"]["subtitle-language-codes"],
              size: game["File Size"],
              platforms: game_info["attributes"]["platforms"],
              publisher: game_info["attributes"]["provider-name"],
              publishDate: game_info["attributes"]["release-date"],
              cover: game_info["attributes"]["thumbnail-url-base"],
             )

  if game_result.valid?
    game_result.save
  else
    puts game_result.errors.messages
    require 'pry'; binding.pry
  end
end
